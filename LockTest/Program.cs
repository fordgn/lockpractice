﻿using System;
using System.Threading;

namespace LockTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var buff = new Meto();
            var buff2 = new Meto();
            Thread t = new Thread(new ParameterizedThreadStart(buff.mainMethod));
            t.Start("abc");
            Thread t1 = new Thread(new ParameterizedThreadStart(buff2.mainMethod));
            t1.Start("def");

        }
        
    }

    public class Meto {

        static int i = 0;
        static object lockObject = new object();
        public void mainMethod(object ti)
        {
            try
            {
                while (i < 100)
                {
                    lock (lockObject)
                    {
                        Console.WriteLine("Thread " + ti.ToString() + " " + i.ToString());
                        i++;
                        if (i == 50)
                        {
                            throw new Exception("EXXX");
                        }
                    }


                }
            }
            catch (Exception ex)
            {

            }
           

            Console.ReadKey();

        }
    }
}
